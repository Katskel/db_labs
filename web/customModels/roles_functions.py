class Role:
    def __init__(self, id, name):
        self.id = id
        self.name = name


class Function:
    def __init__(self, id, name):
        self.id = id
        self.name = name


class FunctionRoles:
    def __init__(self, function_id, role_id):
        self.function_id = function_id
        self.role_id = role_id


class UsersRoles:
    def __init__(self, user_id, role_id):
        self.role_id = role_id
        self.user_id = user_id


class UsersFunctions:
    def __init__(self, user_id, function_id):
        self.user_id = user_id
        self.function_id = function_id


