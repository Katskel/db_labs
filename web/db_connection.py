from web.customModels.custom_models import (
    Document,
    Department,
    DocumentVersion,
    Author,
    User,
    AccessLevel
)

from web.customModels.custom_log_models import (
    RequestLog,
	DocumentLog,
	UserLog,
	DocumentAction
)

import cx_Oracle
import itertools
from typing import List, Dict
from .exceptions import ShownException

default_config = {'user': 'C##django', 'password': 'DJango123', 'database': 'orcl', 'host': 'localhost', 'port': 1521}


# def create_if_not_exists(configuration=default_config):
#     with pymssql.connect(**configuration) as conn:
#         with conn.cursor() as cursor:
#             cursor.execute(r"SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE'")
#             table_names = {c for c in cursor}
#             if table_names != {'Map_FuncRole', 'Version', '"User"', 'Document', 'Role', 'DocumentLog', 'UserLog', 'Department', 'AccessLevel', 'Auther', 'DocumentActions', 'Func', 'RequestLog', 'Map_UserVersion', 'Map_UserRole', 'Map_FuncUser', 'Map_AutherVersion'}:
#
#             with open('web/sql_scripts/create_tables.sql') as query:
#                 conn.commit()


def _raise_if_none(obj, message: str):
    if obj is None:
        raise ShownException(message)
    return obj


class ProxyDbQuery:
    def __init__(self, func, args_selector):
        self.func = func
        self.args_selector = args_selector
        self.cached_args = None
        self.cached_value = None

    def __get__(self, instance, owner):
        # print("PROXY")
        args = self.args_selector(instance)
        self.cached_args = args
        self.cached_value = self.func(*args)
        return self.cached_value

    def __set__(self, instance, value):
        raise AttributeError("Not yet implemented")

	
class Connection:
    __connection = None

    @classmethod
    def get_connection(cls, configuration=default_config):
        if cls.__connection is None:
            cls.__connection = Connection(configuration)
        return cls.__connection

    @classmethod
    def dispose(cls):
        if cls.__connection is not None:
            del cls.__connection
            cls.__connection = None

    def __init__(self, configuration):
        self.config = configuration
        self.connection = cx_Oracle.connect(r'C##django/DJango123@127.0.0.1:1521/orcl')
        self.cursor = self.connection.cursor()

    def __delete__(self, instance):
        self.cursor.close()
        self.connection.close()

    def create_tables(self) -> None:
        with open('web/sql_scripts/create_tables.sql', 'r') as query:
            self.cursor.execute(query.read())
            self.connection.commit()
        with open('web/sql_scripts/init.sql', 'r') as query:
            self.cursor.execute(query.read())
            self.connection.commit()

    def _add_test_data(self) -> None:
        with open('web/sql_scripts/fill_test_data.sql', 'r') as query:
            self.cursor.execute(query.read())
            self.connection.commit()

    def login(self, user_id) -> None:
        self.cursor.callproc('app.login', [user_id])
        self.connection.commit()

    def logout(self) -> None:
        self.cursor.callproc('app.logout')
        self.connection.commit()

    def get_all_departments(self) -> List[Department]:
        departments = self.cursor.callfunc('app.get_departments', cx_Oracle.CURSOR)
        return [Department(*item) for item in departments.fetchall()]

    def get_all_documents(self) -> List[Document]:
        docs = self.cursor.callfunc('app.get_all_documents', cx_Oracle.CURSOR)
        return [Document(*item) for item in docs.fetchall()]

    def get_user_by_id(self, user_id: int) -> User:
        users = self.cursor.callfunc('app.get_user_by_id', cx_Oracle.CURSOR, [user_id])
        return User(*_raise_if_none(users.fetchone(), f"No user with such id exist"))

    def get_version_by_id(self, version_id: int) -> DocumentVersion:
        versions = self.cursor.callfunc('app.get_version_by_id', cx_Oracle.CURSOR, [int(version_id)])
        return DocumentVersion(*_raise_if_none(versions.fetchone(), "Version with such id does not exist"))

    def get_access_level_by_id(self, access_id: int) -> str:
        lvls = self.cursor.callfunc('app.get_access_level_by_id', cx_Oracle.CURSOR, [access_id])
        return _raise_if_none(lvls.fetchone(), "No access level with such id exist")[0]

    def get_department_by_id(self, department_id: int) -> Department:
        depts = self.cursor.callfunc('app.get_department_by_id', cx_Oracle.CURSOR, [department_id])
        return Department(*_raise_if_none(depts.fetchone(), "No department with such id exist"))

    def get_document_by_id(self, document_id: int) -> Document:
        docs = self.cursor.callfunc('app.get_document_by_id', cx_Oracle.CURSOR, [document_id])
        return Document(*_raise_if_none(docs.fetchone(), "No document with such id exist"))

    def get_author_by_id(self, author_id: int) -> Author:
        authors = self.cursor.callfunc('app.get_author_by_id', cx_Oracle.CURSOR, [author_id])
        return Author(*_raise_if_none(authors.fetchone(), "No author with such id exist"))

    def get_version_authors(self, version_id: int) -> List[Author]:
        authors = self.cursor.callfunc('app.get_version_authors', cx_Oracle.CURSOR, [version_id])
        return [Author(*a) for a in authors.fetchall()]

    def get_document_versions_extended(self, document_id: int) -> List[DocumentVersion]:
        versions = self.cursor.callfunc('app.get_document_versions_extended', cx_Oracle.CURSOR, [document_id])
        return [DocumentVersion(*item) for item in versions.fetchall()]

    def get_documents_with_versions(self, department_id: int) -> List[Dict]:
        vers = self.cursor.callfunc('app.get_documents_with_versions', cx_Oracle.CURSOR, [department_id])
        versions = [DocumentVersion(*item) for item in vers.fetchall()]
        return [{'versions': list(versions), 'document': self.get_document_by_id(doc_id)}
                for doc_id, versions in itertools.groupby(versions, lambda v: v.document_id)]

    def register_user(self, user_id: int) -> None:
        self.cursor.callproc('app.register_user', [user_id])

    def user_has_permission(self, permission: str) -> bool:
        return self.cursor.callfunc('app.user_has_permission', bool, [permission])

    def get_all_access_levels(self) -> List[AccessLevel]:
        return [AccessLevel(*lvl) for lvl in self.cursor.callfunc('app.get_all_access_levels', cx_Oracle.CURSOR).fetchall()]

    def get_all_authors(self) -> List[Author]:
        auths = self.cursor.callfunc('app.get_all_authors', cx_Oracle.CURSOR)
        return [Author(*auth) for auth in auths.fetchall()]

    def update_version(self, version: DocumentVersion, user_id: int) -> None:
        self.cursor.callproc('app.update_version', [version.creation_date,
                             version.access_id, version.document_id, version.department_id, version.id])

    def _raise_if_author_not_unique(self, author: Author) -> None:
        self.cursor.execute("select count(*) as cnt from Auther where name=:1 and birthdate=:2",
                            (author.name, author.birthdate))
        if self.cursor.fetchone()['cnt'] > 0:
            raise ShownException("Author with this name and birthdate already exists")

    def update_author(self, author: Author) -> None:
        self.cursor.callproc('app.update_author', [author.name, author.biography, author.birthdate, author.id])

    def version_add_author(self, version_id: int, author_id: int, user_id: int) -> None:
        self.cursor.callproc('app.version_add_author', [version_id, author_id])

    def version_remove_author(self, version_id: int, author_id: int, user_id: int) -> None:
        self.cursor.callproc('app.version_remove_author', [version_id, author_id])

    def _log_user_request(self, user_id: int, version_id: int) -> None:
        self.cursor.execute("""insert into RequestLog(user_id, request_time, requested_version_id)
        values(:1, (select sysdate from dual), :2)""", (user_id, version_id))
        self.connection.commit()

    def _log_user_access_changed(self, master_id: int, slave_id: int, access_level: int) -> None:
        self.cursor.execute("""insert into UserLog(master_id, slave_id, change_time, new_access_level)
        values(:1, :2, (select sysdate from dual), :3)""", (master_id, slave_id, access_level))
        self.connection.commit()

    def _log_document_action(self, user_id: int, version_id: int, action: str) -> None:
        return
        self.cursor.execute("select id from DocumentActions where title=:1", (action, ))
        act = self.cursor.fetchone()
        if act is None:
            self.cursor.execute("insert into DocumentActions(title) values(:1)", (action, ))
            self.connection.commit()
            act = self.cursor.lastrowid
        else:
            act = act['id']
        self.cursor.execute("""insert into DocumentLog(user_id, version_id, action, change_time)
        values(:1, :2, :3, (select sysdate from dual))""", (user_id, version_id, act))
        self.connection.commit()

    def update_department(self, department: Department) -> None:
        self.cursor.callproc('app.update_department', [department.address, department.name, department.id])

    def add_department(self, department: Department) -> None:
        self.cursor.callproc('app.add_department', [department.address, department.name])

    def update_document(self, document: Document) -> None:
        self.cursor.callproc('app.update_document', [document.name, document.description, document.id])

    def add_document(self, document: Document) -> None:
        self.cursor.callproc('app.add_document', [document.name, document.description])

    def add_version(self, version: DocumentVersion) -> None:
        self.cursor.callproc('app.add_version', 
            [version.creation_date, version.document_id, version.access_id, version.department_id])

    def add_author(self, author: Author) -> None:
        self.cursor.callproc('app.add_author', [author.name, author.birthdate, author.biography])

    def _raise_if_no_level(self, user_id: int, version_id: int) -> None:
        user = self.get_user_by_id(user_id)
        self.cursor.execute("""select count(*) as cnt
            from Version v left join Map_UserVersion m on v.id=m.version_id
            where v.id=:1 and (security_level <= :2 or (m.user_id=:3 and m.access_grantes = 1))""",
                            (version_id, user.access_id, user_id))
        if self.cursor.fetchone()['cnt'] == 0:
            raise ShownException("Version with such id does not exist!")

    def change_pdf(self, version_id: int, pdf_path: str) -> None:
        self.cursor.callproc('app.change_pdf', (pdf_path, int(version_id)))

    def get_pdf(self, version_id: int) -> str:
        return self.cursor.callfunc('app.get_pdf', str, [version_id])

    def get_request_logs(self):
        logs = self.cursor.callfunc('app.get_request_logs', cx_Oracle.CURSOR)
        return [RequestLog(*log) for log in logs.fetchall()]

    def get_document_logs(self):
        logs = self.cursor.callfunc('app.get_document_logs', cx_Oracle.CURSOR)
        return [DocumentLog(*log) for log in logs.fetchall()]


DocumentVersion.department = ProxyDbQuery(Connection.get_connection().get_department_by_id,
                                          lambda v: (v.department_id,))
DocumentVersion.access_level = ProxyDbQuery(Connection.get_connection().get_access_level_by_id,
                                            lambda v: (v.access_id,))
DocumentVersion.authors = ProxyDbQuery(Connection.get_connection().get_version_authors, lambda v: (v.id,))
DocumentVersion.document = ProxyDbQuery(Connection.get_connection().get_document_by_id, lambda v: (v.document_id,))
User.access_level = ProxyDbQuery(Connection.get_connection().get_access_level_by_id, lambda u: (u.access_id,))
