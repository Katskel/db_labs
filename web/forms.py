from django.http import HttpResponseRedirect
from django.views.generic.base import View
from django.views.generic.edit import FormView
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login, logout
from django.contrib.auth.models import User
from django.forms import Form
from django import forms
from .db_connection import Connection


class RegisterFormView(FormView):
    form_class = UserCreationForm
    success_url = "/tasks/"
    template_name = "register.html"

    def form_valid(self, form):
        form.save()
        name = form.cleaned_data['username']
        # create our user here
        super(RegisterFormView, self).form_valid(form)
        user = User.objects.get(username=name)
        conn = Connection.get_connection()
        conn.register_user(user.id)
        login(self.request, user)
        return HttpResponseRedirect("/documents/")


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = "login.html"
    success_url = "/versions/"

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        conn = Connection.get_connection()
        conn.login(self.user.id)
        super(LoginFormView, self).form_valid(form)
        return HttpResponseRedirect("/documents/")


class LogoutView(View):
    def get(self, request):
        logout(request)
        conn = Connection.get_connection()
        conn.logout()
        return HttpResponseRedirect('/login/')


class DocumentVersionWriteForm(Form):
    level_access = forms.IntegerField(widget=forms.Select(attrs={}), required=True)
    department_id = forms.IntegerField(widget=forms.Select(attrs={}), required=True)
    document_id = forms.IntegerField(widget=forms.Select(attrs={}), required=True)

    creation_date = forms.DateField(required=True, widget=forms.SelectDateWidget)
    pdf = forms.FileField(widget=forms.ClearableFileInput(), required=False)


class DocumentVersionReadForm(Form):
    level_access = forms.CharField(max_length=20, disabled=True)
    creation_date = forms.DateField(disabled=True)

    department_id = forms.IntegerField()
    department_name = forms.CharField(max_length=40, disabled=True)

    document_id = forms.IntegerField()
    document_name = forms.CharField(max_length=30, disabled=True)


class DocumentReadForm(Form):
    id = forms.IntegerField()
    name = forms.CharField(max_length=30, disabled=True)
    description = forms.CharField(max_length=30, disabled=True)


class DocumentWriteForm(Form):
    name = forms.CharField(max_length=30, required=True)
    description = forms.CharField(max_length=30, required=True)


class DepartmentReadForm(Form):
    id = forms.IntegerField()
    name = forms.CharField(max_length=30, disabled=True)
    address = forms.CharField(max_length=30, disabled=True)


class DepartmentWriteForm(Form):
    name = forms.CharField(max_length=30, required=True)
    address = forms.CharField(max_length=30, required=True)


class AuthorReadForm(Form):
    name = forms.CharField(max_length=30, disabled=True)
    biography = forms.CharField(max_length=30, disabled=True)
    birthdate = forms.DateField(disabled=True)


class AuthorWriteForm(Form):
    name = forms.CharField(max_length=30, required=True)
    biography = forms.CharField(max_length=30)
    birthdate = forms.DateField(required=True, widget=forms.SelectDateWidget)
