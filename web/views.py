from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, Http404
from django.urls import reverse
from web import forms, db_connection
from web.utils import exceptions_wrapper
import datetime
from .utils import has_permission, login_required
import os
from web.customModels import custom_models
from django.core.files import File


@exceptions_wrapper
def index(request):
    template = loader.get_template('index.html')
    context = {'user_login': request.user.username}
    return HttpResponse(template.render(context))


@exceptions_wrapper
@login_required
def user_documents(request):
    conn = db_connection.Connection.get_connection()
    template = loader.get_template('documents.html')
    context = {'documents': conn.get_all_documents(),
               'user_login': request.user.username,
               'can_add_document': conn.user_has_permission('create document'),
               'can_edit_document': conn.user_has_permission('edit document')
               }
    return HttpResponse(template.render(context))


@exceptions_wrapper
@login_required
def user_departments(request):
    conn = db_connection.Connection.get_connection()
    departments = conn.get_all_departments()
    template = loader.get_template('departments.html')
    context = {'departments': departments,
               'user_login': request.user.username,
               'can_add_department': conn.user_has_permission('create department'),
               'can_edit_department': conn.user_has_permission('edit department')
               }
    return HttpResponse(template.render(context))


@exceptions_wrapper
@login_required
def user_authors(request):
    conn = db_connection.Connection.get_connection()
    authors = conn.get_all_authors()
    template = loader.get_template('authors.html')
    context = {'authors': authors,
               'user_login': request.user.username,
               'can_add_author': conn.user_has_permission('create author'),
               'can_edit_author': conn.user_has_permission('edit author')
               }
    return HttpResponse(template.render(context))


@login_required
@exceptions_wrapper
def version_read(request, version_id):
    conn = db_connection.Connection.get_connection()
    version = conn.get_version_by_id(version_id)
    form = forms.DocumentVersionReadForm(initial={
        'level_access': version.access_level,
        'creation_date': version.creation_date or datetime.datetime.now(),
        'department_id': version.department_id,
        'document_id': version.document_id,
        'department_name': version.department.name,
        'document_name': version.document.name,
    })
    template = loader.get_template('version_read.html')
    context = {'form': form,
               'authors': conn.get_version_authors(version_id),
               'user_login': request.user.username,
               'version_id': version_id,
               'can_edit_version': conn.user_has_permission('edit version'),
               'can_edit_author': conn.user_has_permission('edit author'),
               'has_file': version.pdf is not None
               }
    return HttpResponse(template.render(context))


@login_required
@exceptions_wrapper
@has_permission('edit version')
def version_write(request, version_id):
    conn = db_connection.Connection.get_connection()
    version = conn.get_version_by_id(version_id)
    version_authors = version.authors
    departments = conn.get_all_departments()
    documents = conn.get_all_documents()
    access_levels = conn.get_all_access_levels()
    authors = conn.get_all_authors()
    if request.method == 'POST':
        form = forms.DocumentVersionWriteForm(request.POST)
        if form.is_valid():
            version.creation_date = form.cleaned_data['creation_date']
            version.access_id = form.cleaned_data['level_access']
            version.department_id = form.cleaned_data['department_id']
            version.document_id = form.cleaned_data['document_id']
            conn.update_version(version, request.user.id)
            if 'pdf' in request.FILES.keys():
                print(type(request.FILES['pdf']))
                dirs = f"./pdfs/{version.id}/"
                if not os.path.exists(dirs):
                    os.makedirs(dirs)
                cnt = len(os.listdir(dirs))
                path = f"./pdfs/{version.id}/{cnt}__{str(request.FILES['pdf'])}"
                with open(path, 'wb+') as destination:
                    for chunk in request.FILES['pdf'].chunks():
                        destination.write(chunk)
                conn.change_pdf(version_id, path)
            return HttpResponseRedirect(reverse('documents', args=()))
    else:
        if version.pdf:
            version.pdf = File(open(version.pdf))
            version.pdf.url = '2'
        version.level_access = version.access_id
        form = forms.DocumentVersionWriteForm(initial=vars(version))
        print(vars(version))

    form.fields['level_access'].widget.choices = [(level.id, level.name) for level in access_levels]
    form.fields['department_id'].widget.choices = [(department.id, department.name) for department in departments]
    form.fields['document_id'].widget.choices = [(document.id, document.name) for document in documents]
    form.fields['pdf'].widget.initial = version.pdf

    return render(request, 'version_write.html', {
        'form': form,
        'user_login': request.user.username,
        'version_id': version_id,
        'version_authors': version_authors,
        'authors': authors,
        'can_edit_author': conn.user_has_permission('edit author'),
        'can_edit_authority': conn.user_has_permission('edit authority')
    })


@login_required
@exceptions_wrapper
def version_download(request, version_id):
    conn = db_connection.Connection.get_connection()
    file_path = conn.get_pdf(version_id)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/force-download")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    return Http404()


@login_required
@exceptions_wrapper
def document_read(request, document_id):
    conn = db_connection.Connection.get_connection()
    document = conn.get_document_by_id(document_id)
    document_versions = conn.get_document_versions_extended(document_id)
    form = forms.DocumentReadForm(initial=vars(document))
    template = loader.get_template('document_read.html')
    context = {'form': form,
               'versions': document_versions,
               'user_login': request.user.username,
               'document_id': document_id,
               'can_edit_document': conn.user_has_permission('edit document'),
               'can_edit_version': conn.user_has_permission('edit version')
               }
    return HttpResponse(template.render(context))


@login_required
@exceptions_wrapper
def department_read(request, department_id):
    conn = db_connection.Connection.get_connection()
    department = conn.get_department_by_id(department_id)
    form = forms.DepartmentReadForm(initial=vars(department))
    template = loader.get_template('department_read.html')
    context = {'form': form,
               'document_versions': conn.get_documents_with_versions(department_id),
               'user_login': request.user.username,
               'department_id': department_id,
               'can_edit_department': conn.user_has_permission('edit department'),
               'can_edit_version': conn.user_has_permission('edit version'),
               'can_edit_document': conn.user_has_permission('edit document')
               }
    return HttpResponse(template.render(context))


@login_required
@exceptions_wrapper
def author_read(request, author_id):
    conn = db_connection.Connection.get_connection()
    author = conn.get_author_by_id(author_id)
    form = forms.AuthorReadForm(initial=vars(author))
    template = loader.get_template('author_read.html')
    context = {'form': form,
               'user_login': request.user.username,
               'author_id': author_id,
               'can_edit_author': conn.user_has_permission('edit author')
               }
    return HttpResponse(template.render(context))


@login_required
@exceptions_wrapper
@has_permission('edit author')
def author_write(request, author_id):
    conn = db_connection.Connection.get_connection()
    author = conn.get_author_by_id(author_id)
    if request.method == 'POST':
        form = forms.AuthorWriteForm(request.POST)
        if form.is_valid():
            author.name = form.cleaned_data['name']
            author.biography = form.cleaned_data['biography']
            author.birthdate = form.cleaned_data['birthdate']
            conn.update_author(author)
            return HttpResponseRedirect(reverse('authors', args=()))
    else:
        form = forms.AuthorWriteForm(initial=vars(author))

    return render(request, 'author_write.html', {
        'form': form,
        'user_login': request.user.username,
        'write': True,
        'author_id': author_id
    })


@login_required
@exceptions_wrapper
@has_permission('create author')
def author_add(request):
    conn = db_connection.Connection.get_connection()
    if request.method == 'POST':
        form = forms.AuthorWriteForm(request.POST)
        if form.is_valid():
            author = custom_models.Author(-1, '', '', None)
            author.name = form.cleaned_data['name']
            author.biography = form.cleaned_data['biography']
            author.birthdate = form.cleaned_data['birthdate']
            conn.add_author(author)
            return HttpResponseRedirect(reverse('authors', args=()))
    else:
        form = forms.AuthorWriteForm()

    return render(request, 'author_write.html', {
        'form': form,
        'write': False,
        'user_login': request.user.username
    })


@login_required
@exceptions_wrapper
@has_permission('edit authority')
def version_remove_author(request, version_id, author_id):
    conn = db_connection.Connection.get_connection()
    conn.version_remove_author(version_id, author_id, request.user.id)
    return HttpResponseRedirect(reverse('version_write', args=(version_id,)))


@login_required
@exceptions_wrapper
@has_permission('edit authority')
def version_add_author(request, version_id):
    if request.method == "POST":
        author_id = request.POST.get('author')
        if author_id:
            conn = db_connection.Connection.get_connection()
            conn.version_add_author(version_id, author_id, request.user.id)
    return HttpResponseRedirect(reverse('version_write', args=(version_id,)))


@login_required
@exceptions_wrapper
@has_permission('edit department')
def department_write(request, department_id):
    conn = db_connection.Connection.get_connection()
    department = conn.get_department_by_id(department_id)
    if request.method == 'POST':
        form = forms.DepartmentWriteForm(request.POST)
        if form.is_valid():
            department.name = form.cleaned_data['name']
            department.address = form.cleaned_data['address']
            conn.update_department(department)
            return HttpResponseRedirect(reverse('departments', args=()))
    else:
        form = forms.DepartmentWriteForm(initial=vars(department))

    return render(request, 'department_write.html', {
        'form': form,
        'write': True,
        'department_id': department_id,
        'user_login': request.user.username
    })


@login_required
@exceptions_wrapper
@has_permission('create department')
def department_add(request):
    if request.method == 'POST':
        form = forms.DepartmentWriteForm(request.POST)
        if form.is_valid():
            department = custom_models.Department(-1, '', '')
            department.name = form.cleaned_data['name']
            department.address = form.cleaned_data['address']
            conn = db_connection.Connection.get_connection()
            conn.add_department(department)
            return HttpResponseRedirect(reverse('departments', args=()))
    else:
        form = forms.DepartmentWriteForm()

    return render(request, 'department_write.html', {
        'form': form,
        'write': False,
        'user_login': request.user.username
    })


@login_required
@exceptions_wrapper
@has_permission('edit document')
def document_write(request, document_id):
    conn = db_connection.Connection.get_connection()
    document = conn.get_document_by_id(document_id)
    if request.method == 'POST':
        form = forms.DocumentWriteForm(request.POST)
        if form.is_valid():
            document.name = form.cleaned_data['name']
            document.description = form.cleaned_data['description']
            conn.update_document(document)
            return HttpResponseRedirect(reverse('documents', args=()))
    else:
        form = forms.DocumentWriteForm(initial=vars(document))

    return render(request, 'document_write.html', {
        'form': form,
        'user_login': request.user.username,
        'document_id': document_id,
        'can_add_version': conn.user_has_permission('create version'),
    })


@login_required
@exceptions_wrapper
@has_permission('create document')
def document_add(request):
    if request.method == 'POST':
        form = forms.DocumentWriteForm(request.POST)
        if form.is_valid():
            document = custom_models.Document(-1, '', '')
            document.name = form.cleaned_data['name']
            document.description = form.cleaned_data['description']
            conn = db_connection.Connection.get_connection()
            conn.add_document(document)
            return HttpResponseRedirect(reverse('documents', args=()))
    else:
        form = forms.DocumentWriteForm()

    return render(request, 'document_add.html', {
        'form': form,
        'user_login': request.user.username
    })


@login_required
@exceptions_wrapper
@has_permission('create version')
def version_add(request, document_id):
    conn = db_connection.Connection.get_connection()
    departments = conn.get_all_departments()
    documents = conn.get_all_documents()
    access_levels = conn.get_all_access_levels()
    if request.method == 'POST':
        form = forms.DocumentVersionWriteForm(request.POST)
        if form.is_valid():
            version = custom_models.DocumentVersion(-1, None, 0, None, 0, 0)
            version.creation_date = form.cleaned_data['creation_date']
            version.access_id = form.cleaned_data['level_access']
            version.department_id = form.cleaned_data['department_id']
            version.document_id = form.cleaned_data['document_id']
            conn.add_version(version)
            return HttpResponseRedirect(reverse('document_write', args=(document_id,)))
    else:
        form = forms.DocumentVersionWriteForm(initial={'document_id': document_id})

    form.fields['level_access'].widget.choices = [(level.id, level.name) for level in access_levels]
    form.fields['department_id'].widget.choices = [(department.id, department.name) for department in departments]
    form.fields['document_id'].widget.choices = [(document.id, document.name) for document in documents]

    return render(request, 'version_add.html', {
        'form': form,
        'user_login': request.user.username,
    })


@login_required
@exceptions_wrapper
def all_document_logs(request):
    conn = db_connection.Connection.get_connection()
    logs = conn.get_document_logs()
    template = loader.get_template('document_log.html')
    context = {'logs': logs,
               'user_login': request.user.username,
               }
    return HttpResponse(template.render(context))


@login_required
@exceptions_wrapper
def all_request_logs(request):
    conn = db_connection.Connection.get_connection()
    logs = conn.get_request_logs()
    template = loader.get_template('request_log.html')
    context = {'logs': logs,
               'user_login': request.user.username,
               }
    return HttpResponse(template.render(context))

