drop table if exists
	  Map_FuncUser
	 ,Map_FuncRole
	 ,Map_UserRole
	 ,Map_AutherVersion
	 ,Map_UserVersion
	 ,DocumentLog
	 ,RequestLog
	 ,UserLog
	 ,"User"
	 ,Version
	 ,Department
	 ,Func
	 ,Role
	 ,AccessLevel
	 ,Auther
	 ,Document
	 ,DocumentActions;

create table Func(
	 id tinyint identity(1,1) not null primary key
	,title nvarchar(20) unique not null
);

create table Role(
	 id tinyint identity(1,1) not null primary key
	,name nvarchar(20) unique not null
);

create table "User"(
	 id int not null primary key
	--,email nvarchar(256) unique not null
	--,login nvarchar(50) unique not null
	--,password_hash char(16) not null
	,access_level tinyint not null
);

create table Department(
     id int identity(1,1) not null primary key
    ,address nvarchar(256) not null
    ,name nvarchar(256) unique not null
);

create table AccessLevel(
	 id tinyint identity(1,1) not null primary key
	,level nvarchar(20) unique not null
);

create table Auther(
	 id int identity(1,1) not null primary key
	,name nvarchar(100) not null
	,birthdate date null
	,biography ntext null
	,constraint auther_unique unique(name, birthdate)
);

create table Document(
	 id int identity(1,1) not null primary key
	,title nvarchar(100) not null
	,description nvarchar(500) null
);

create table Version(
	 id int identity(1,1) not null primary key
	,release_time datetime null
	,scan varbinary(max) null
	,document_id int not null
	,security_level tinyint not null
	,department_id int not null
);

create table UserLog(
	 id int identity(1,1) not null primary key
	,change_time datetime not null
	,master_id int not null
	,slave_id int not null
	,new_access_level tinyint not null
);

create table RequestLog(
	 id int identity(1,1) not null primary key
	,user_id int not null
	,request_time datetime not null
	,requested_version_id int not null
);

create table DocumentActions(
	 id tinyint identity(1,1) not null primary key
	,title nvarchar(20) unique not null
);

create table DocumentLog(
	 id int identity(1,1) not null primary key
	,user_id int not null
	,version_id int not null
	,action tinyint not null
	,change_time datetime not null
);

alter table "User"
	add foreign key(access_level) references AccessLevel(id);

alter table Version
	add foreign key(document_id) references Document(id);
alter table Version
	add foreign key(security_level) references AccessLevel(id);
alter table Version
	add foreign key(department_id) references Department(id);

alter table UserLog
	add foreign key(master_id) references "User"(id);
alter table UserLog
	add foreign key(slave_id) references "User"(id);
alter table UserLog
	add foreign key(new_access_level) references AccessLevel(id);

alter table RequestLog
	add foreign key(user_id) references "User"(id);
alter table RequestLog
	add foreign key(requested_version_id) references Version(id);

alter table DocumentLog
	add foreign key(user_id) references "User"(id);
alter table DocumentLog
	add foreign key(version_id) references Version(id);
alter table DocumentLog
	add foreign key(action) references DocumentActions(id);

create table Map_FuncUser(
	 user_id int not null foreign key references "User"(id)
	,func_id tinyint not null foreign key references Func(id)
	,primary key(user_id, func_id)
);

create table Map_FuncRole(
	 role_id tinyint not null foreign key references Role(id)
	,func_id tinyint not null foreign key references Func(id)
	,primary key(role_id, func_id)
);

create table Map_UserRole(
	 role_id tinyint not null foreign key references Role(id)
	,user_id int not null foreign key references "User"(id)
	,primary key(role_id, user_id)
);

create table Map_AutherVersion(
	 auther_id int not null foreign key references Auther(id)
	,version_id int not null foreign key references Version(id)
	,primary key(auther_id, version_id)
);

create table Map_UserVersion(
	 user_id int not null foreign key references "User"(id)
	,version_id int not null foreign key references Version(id)
	,access_grantes bit not null
	,primary key(user_id, version_id)
);