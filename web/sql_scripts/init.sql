insert into AccessLevel(lvl) values ('public');
insert into AccessLevel(lvl) values('confidential');
insert into AccessLevel(lvl) values('secret');
insert into AccessLevel(lvl) values('top secret');

insert into Role(name) values ('data manager');

insert into Func(title) values('edit author');
insert into Func(title) values('edit authority');
insert into Func(title) values('edit version');

insert into Func(title) values('edit department');
insert into Func(title) values('edit document');
insert into Func(title) values('create document');
insert into Func(title) values('create version');

insert into Func(title) values('create author');
insert into Func(title) values('create department');
insert into Func(title) values('change pdf');
insert into Func(title) values('view logs');

insert into Map_FuncRole(func_id, role_id) values(1, 1);
insert into Map_FuncRole(func_id, role_id) values(2, 1);
insert into Map_FuncRole(func_id, role_id) values(3, 1);
insert into Map_FuncRole(func_id, role_id) values(4, 1);
insert into Map_FuncRole(func_id, role_id) values(5, 1);
insert into Map_FuncRole(func_id, role_id) values(6, 1);
insert into Map_FuncRole(func_id, role_id) values(7, 1);
insert into Map_FuncRole(func_id, role_id) values(8, 1);
insert into Map_FuncRole(func_id, role_id) values(9, 1);
insert into Map_FuncRole(func_id, role_id) values(10, 1);
insert into Map_FuncRole(func_id, role_id) values(11, 1);

insert into DocumentActions(title) values('create');
insert into DocumentActions(title) values('edit');
insert into DocumentActions(title) values('add version');
