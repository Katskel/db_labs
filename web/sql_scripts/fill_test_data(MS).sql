insert into department(address, name)
values('adr1', 'dept1'), ('adr2', 'dept2'), ('adr3', 'dept3');

insert into auther(name, birthdate, biography)
values ('auth1', '2000-01-01', 'bio1'), ('auth2', '2002-02-02', 'bio2');

insert into document(title, description)
values ('doc1', 'descr1'), ('doc2', 'descr2');

insert into version(document_id, security_level, department_id)
values (1, 1, 1), (1, 2, 1), (1, 3, 2), (2, 2, 2), (2, 3, 1), (2, 4, 1);

insert into Map_AutherVersion(auther_id, version_id)
values(1, 1), (1, 2), (1, 3), (2, 3), (2, 1), (2, 6);